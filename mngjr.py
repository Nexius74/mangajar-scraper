import os
import re
import shutil
import logging
import requests
from uuid import uuid4
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from concurrent.futures import ThreadPoolExecutor


FORMAT = '[%(levelname)s] : %(asctime)-15s : %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)


class MangajarScraper(object):
    """

    """
    BASE_URL = 'https://mangajar.com'
    API_PAGE_URL = 'https://static.mangajar.com/pages/'

    def __init__(self, debug: bool = False):
        """
        :param debug:
        """
        self.__s = requests.Session()
        self.__debug = debug
        self.__id = uuid4()

    # Hidden methods
    def __parse_name(self, soup: BeautifulSoup):
        """
        :param soup:
        :return:
        """
        self.name = self.sanitize_string(soup.find('span', {'class': 'post-name'}).text)

    def __parse_description(self, soup: BeautifulSoup):
        """
        :param soup:
        :return:
        """
        self.description = self.sanitize_string_from_tags(soup.find('div', {'class': 'manga-description'}).text).strip()

    def __parse_thumbnail_url(self, soup: BeautifulSoup):
        """
        :param soup:
        :return:
        """
        self.thumbnail_url = soup.find('img', {'class': 'img-fluid'})['src']

    def __parse_chapters_url(self, soup: BeautifulSoup):
        """
        :param soup:
        :return:
        """
        self.chapters_url = []
        self.chapters_url.append(soup.find('div', {'class': 'chaptersListLoader'})['data-src'])

    # Object methods
    def fetch(self, url: str):
        """
        :param url:
        :return: self
        """
        # Check url
        self.parse_url(url)
        # Request url
        res = self.__s.get(url)
        if res.status_code != 200:
            raise RuntimeError(f'Unable to contact {url}')

        soup = BeautifulSoup(res.content, 'html.parser')
        # Parsing soup
        self.__parse_name(soup)
        self.__parse_description(soup)
        self.__parse_thumbnail_url(soup)
        self.__parse_chapters_url(soup)

        return self

    def fetch_chapters_data(self):
        """
        Fetch chapters data as they're stored in another url on mangajar

        :return: zip object
        """
        chapters_url = []
        chapters_name = []

        if self.chapters_url:
            url = f'{self.BASE_URL}{self.chapters_url[0]}'
            res = self.__s.get(url)
            if res.status_code != 200:
                raise RuntimeError(f'Unable to contact {url}')

            soup = BeautifulSoup(res.content, 'html.parser')
            # Parsing chapters name + url
            for x in soup.find_all('li', {'class': 'chapter-item'}):
                chapters_url.append(x.a['href'])
                chapters_name.append(self.sanitize_string(' '.join(x.a.text.split())))

            # Order list
            chapters_url.reverse()
            chapters_name.reverse()
        else:
            raise RuntimeError('.fetch() must be called before .fetch_chapters_data()')

        return zip(chapters_name, chapters_url)

    def download(self, url: str, dest: str = './downloads'):
        """
        :param url:
        :param dest: (Optional)
        :return:
        """
        self.fetch(url)
        # Build path
        path = os.path.join(dest, self.name)
        # Create dir
        if not os.path.exists(path):
            os.mkdir(path)
        # Get chapters data
        zip_obj = self.fetch_chapters_data()

        # Download chapters
        with ThreadPoolExecutor(max_workers=3) as executor:
            for name, url in zip_obj:
                executor.submit(self.download_chapter, url, name, path)

    def download_chapter(self, url: str, name: str, dest: str = './downloads'):
        """
        :param url:
        :param name:
        :param dest: (Optional)
        :return:
        """
        pages_url = []
        # Build chapter path
        path = os.path.join(dest, name)
        # Create folder
        if not os.path.exists(path):
            os.makedirs(path)

        # Build chapter url
        chapter_url = f'{self.BASE_URL}{url}'
        res = self.__s.get(chapter_url)
        if res.status_code != 200:
            logging.error(f'Unable to download chapter {name}')
        else:
            soup = BeautifulSoup(res.content, 'html.parser')
            # Retrieve all page url
            for img in soup.find_all('img'):
                if self.API_PAGE_URL in img['src']:
                    pages_url.append(img['src'])

            # Check if chapter folder contain as many file as url
            if not self.is_complete(pages_url, path):
                # If not delete chapter and recreate it
                shutil.rmtree(path)
                os.mkdir(path)
                # Then download
                for idx, page_url in enumerate(pages_url, start=1):
                    self.download_page(page_url, idx, path)

    def download_page(self, url: str, name: str, dest: str = './downloads'):
        """
        :param url:
        :param name:
        :param dest: (Optional)
        :return:
        """
        res = self.__s.get(url)
        if res.status_code != 200:
            logging.error(f'Unable to contact page {name}')
        else:
            if name == 1 and not os.path.exists(os.path.join(os.path.dirname(dest), 'thumbnail.jpg')):
                self.download_thumbnail(os.path.dirname(dest))
            with open(os.path.join(dest, f'{name}.jpg'), 'wb') as h:
                h.write(res.content)

    def download_thumbnail(self, dest: str = './downloads'):
        """
        :param dest: (Optional)
        :return:
        """
        if self.thumbnail_url:
            res = self.__s.get(self.thumbnail_url)
            if res.status_code != 200:
                raise RuntimeError('Unable to download manga thumbnail')
            with open(os.path.join(dest, 'thumbnail.jpg'), 'wb') as h:
                h.write(res.content)
        else:
            raise SyntaxError('.fetch() must be called before .download_thumbnail()')

    # Static methods
    @staticmethod
    def is_complete(lst: list, path: str) -> bool:
        """
        :return: bool
        """
        files = os.listdir(path)
        if len(files) != len(lst) or len(files) == 0:
            return False
        return True

    @staticmethod
    def parse_url(url: str):
        """
        Parse an url to check if it correspond to what we want

        :param url:
        :return:
        """
        o = urlparse(url)
        # Test scheme
        if o.scheme != 'https':
            raise SyntaxError(f'HTTPS not found in url: {url}')
        # Test netloc
        if o.netloc != 'www.mangajar.com' and o.netloc != 'mangajar.com':
            raise SyntaxError(f'Invalid url: {url}. E.g. https://mangajar.com/manga/acchi-kochi')
        if '/chapter/' in o.path:
            raise SyntaxError(f'Invalid url: {url}. E.g. https://mangajar.com/manga/acchi-kochi')

    @staticmethod
    def sanitize_string(string: str) -> str:
        """
        Remove forbidden character and escape character from given string

        :param string:
        :return: A formatted string
        """
        return string.replace(':', '').replace('*', 'x').replace('?', '') \
            .replace('<', '').replace('>', '').replace('\t', '').replace('\\', '') \
            .replace('/', '').replace('|', '').replace('[', '(').replace(']', ')') \
            .replace('"', '').replace('.', '').strip()

    @staticmethod
    def sanitize_string_from_tags(string: str) -> str:
        """
        Remove all html tags find in string

        :param string:
        :return: A formatted string
        """
        return re.sub(r'(<\w*>)|(<\/\w*>)|(<\w*\/>)', '', string)
